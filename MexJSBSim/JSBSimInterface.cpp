#include "JSBSimInterface.h"
#include <models/FGAircraft.h>
#include <math/FGStateSpace.h>
#include <math/FGQuaternion.h>

JSBSimInterface::JSBSimInterface(FGFDMExec *fdmex)
{
	_ac_model_loaded = false;
	fdmExec = fdmex;
	propagate = fdmExec->GetPropagate();
	auxiliary = fdmExec->GetAuxiliary();
	aerodynamics = fdmExec->GetAerodynamics();
	propulsion = fdmExec->GetPropulsion();
	fcs = fdmExec->GetFCS();
	ic = new FGInitialCondition(fdmExec);
	verbosityLevel = JSBSimInterface::eSilent;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
JSBSimInterface::~JSBSimInterface(void)
{
	fdmExec = 0L;
	delete ic;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::Open(const mxArray *prhs)
{
	if (!fdmExec) return 0;

	char buf[128];
	mwSize buflen;
	buflen = mxGetNumberOfElements(prhs) + 1;
	mxGetString(prhs, buf, buflen);
	string acName = string(buf);
    string rootDir = ""; // "JSBSim/";

	//mexEvalString("plot(sin(0:.1:pi))");

	if ( fdmExec->GetAircraft()->GetAircraftName() != ""  )
	{
		if ( verbosityLevel == eVerbose )
		{
			mexPrintf("\tERROR: another aircraft is already loaded ('%s').\n", fdmExec->GetAircraft()->GetAircraftName().c_str());
			mexPrintf("\t       To load a new aircraft, clear the mex file and start up again.\n");
		}
		return 0;
	}

	// JSBSim stuff

	if ( verbosityLevel == eVerbose )
		mexPrintf("\tSetting up JSBSim with standard 'aircraft', 'engine', and 'system' paths.\n");

    fdmExec->SetAircraftPath (rootDir + "aircraft");
    fdmExec->SetEnginePath   (rootDir + "engine"  );
    fdmExec->SetSystemsPath  (rootDir + "systems" );

	if ( verbosityLevel == eVerbose )
		mexPrintf("\tLoading aircraft '%s' ...\n",acName.c_str());

    if ( ! fdmExec->LoadModel( rootDir + "aircraft",
                               rootDir + "engine",
                               rootDir + "systems",
                               acName)) 
	{
		_ac_model_loaded = false;
		delete fdmExec;
		if ( verbosityLevel == eVerbose )
			mexPrintf("\tERROR: JSBSim could not load the aircraft model.\n");
		return 0;
    }
	_ac_model_loaded = true;
	// Print AC name
	if ( verbosityLevel == eVerbose )
		mexPrintf("\tModel %s loaded.\n", fdmExec->GetModelName().c_str() );

//***********************************************************************
	// populate aircraft catalog
	catalog = this->SPrintPropertyCatalog();

	if ( verbosityLevel == eVeryVerbose )
	{
		for (unsigned i=0; i<catalog.size(); i++) 
			mexPrintf("%s\n",catalog[i].c_str());
	}
//***********************************************************************/

	return 1;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::GetPropertyValue(const mxArray *prhs1, double& value)
{
	if (!fdmExec) return 0;
	if (!IsAircraftLoaded()) return 0;

	char buf[128];
	mwSize buflen;
	buflen = mxGetNumberOfElements(prhs1) + 1;
	mxGetString(prhs1, buf, buflen);
	string prop = "";
	prop = string(buf);
	if ( !QueryJSBSimProperty(prop) )
	{
		if ( verbosityLevel == eVerbose )
			mexPrintf("\tERROR: JSBSim could not find the property '%s' in the aircraft catalog.\n",prop.c_str());
		return 0;
	}
	value = fdmExec->GetPropertyValue(prop);
	return 1;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::SetPropertyValue(const mxArray *prhs1, const mxArray *prhs2)
{
	if (!fdmExec) return 0;
	if (!IsAircraftLoaded()) return 0;

	char buf[128];
	mwSize buflen;
	buflen = mxGetNumberOfElements(prhs1) + 1;
	mxGetString(prhs1, buf, buflen);
	string prop = "";
	prop = string(buf);
	double value = *mxGetPr(prhs2);

	if (!EasySetValue(prop,value)) // first check if an easy way of setting is implemented
	{
		if ( !QueryJSBSimProperty(prop) ) // then try to set the full-path property, e.g. '/fcs/elevator-cmd-norm'
		{
			if ( verbosityLevel == eVerbose )
				mexPrintf("\tERROR: JSBSim could not find the property '%s' in the aircraft catalog.\n",prop.c_str());
			return 0;
		}
		fdmExec->SetPropertyValue( prop, value );
	}
	return 1;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::SetPropertyValue(const string prop, const double value)
{
		mxArray *p1 = mxCreateString(prop.c_str());
		mxArray *p2 = mxCreateDoubleMatrix(1, 1, mxREAL);
		*mxGetPr(p2) = value;
		return SetPropertyValue(p1,p2);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::EasySetValue(const string prop, const double value)
{
	if (prop == "u-fps")
	{
		ic->SetUBodyFpsIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: U-Body true flight speed (ft/s) = %f\n", ic->GetUBodyFpsIC() );
		return 1;
	}
	else if (prop == "v-fps")
	{
		ic->SetVBodyFpsIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: V-Body true flight speed (ft/s) = %f\n", ic->GetVBodyFpsIC() );
		return 1;
	}
	else if (prop == "w-fps")
	{
		ic->SetVBodyFpsIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: W-Body true flight speed (ft/s) = %f\n", ic->GetWBodyFpsIC() );
		return 1;
	}
	else if (prop == "p-rad_sec")
	{
		ic->SetPRadpsIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: roll rate (rad/s) = %f\n", ic->GetPRadpsIC());
		return 1;
	}
	else if (prop == "q-rad_sec")
	{
		ic->SetQRadpsIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: pitch rate (rad/s) = %f\n", ic->GetQRadpsIC());
		return 1;
	}
	else if (prop == "r-rad_sec")
	{
		ic->SetRRadpsIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: yaw rate (rad/s) = %f\n", ic->GetRRadpsIC());
		return 1;
	}
	else if (prop == "h-sl-ft")
	{
		ic->SetAltitudeASLFtIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: altitude over sea level (mt) = %f\n", ic->GetAltitudeASLFtIC());
		return 1;
	}
	else if (prop == "long-gc-deg")
	{
		ic->SetLongitudeDegIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: geocentric longitude (deg) = %f\n", ic->GetLongitudeDegIC());
		return 1;
	}
	else if (prop == "lat-gc-deg")
	{
		ic->SetLatitudeDegIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: geocentric latitude (deg) = %f\n", ic->GetLatitudeDegIC());
		return 1;
	}
	else if (prop == "phi-rad")
	{
		/*
		FGQuaternion Quat( value, propagate->GetEuler(2), propagate->GetEuler(3) );
		Quat.Normalize();
		FGPropagate::VehicleState vstate = propagate->GetVState();
		vstate.qAttitudeLocal = Quat;
		propagate->SetVState(vstate);
		propagate->Run(false); // vVel => gamma
		auxiliary->Run(false); // alpha, beta, gamma
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: phi -> quaternion = (%f,%f,%f,%f)\n",
				propagate->GetVState().qAttitudeLocal(1),propagate->GetVState().qAttitudeLocal(2),propagate->GetVState().qAttitudeLocal(3),propagate->GetVState().qAttitudeLocal(4));
		*/
		/*
		mexPrintf("\tEasy-set: alpha (deg) = %f,\n\tbeta (deg) = %f,\n\tgamma (deg) = %f\n",
			auxiliary->Getalpha()*180./M_PI,auxiliary->Getbeta()*180./M_PI,auxiliary->GetGamma()*180./M_PI);
		*/
		ic->SetPhiRadIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
		{
			FGQuaternion quat = ic->GetOrientation();
			mexPrintf("\tEasy-set: phi (deg) -> quaternion \n\t\t%f -> (%f,%f,%f,%f)\n",
				ic->GetPhiDegIC(),
				quat(1),quat(2),quat(3),quat(4));
		}
		return 1;
	}
	else if (prop == "theta-rad")
	{
		/*
		FGQuaternion Quat( propagate->GetEuler(1), value, propagate->GetEuler(3) );
		Quat.Normalize();
		FGPropagate::VehicleState vstate = propagate->GetVState();
		vstate.qAttitudeLocal = Quat;
		propagate->SetVState(vstate);
		propagate->Run(false); // vVel => gamma
		auxiliary->Run(false); // alpha, beta, gamma
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: theta -> quaternion = (%f,%f,%f,%f)\n",
				propagate->GetVState().qAttitudeLocal(1),propagate->GetVState().qAttitudeLocal(2),propagate->GetVState().qAttitudeLocal(3),propagate->GetVState().qAttitudeLocal(4));
		*/
		/*
		mexPrintf("\tEasy-set: alpha (deg) = %f,\n\tbeta (deg) = %f,\n\tgamma (deg) = %f\n",
			auxiliary->Getalpha()*180./M_PI,auxiliary->Getbeta()*180./M_PI,auxiliary->GetGamma()*180./M_PI);
		*/
		ic->SetThetaRadIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
		{
			FGQuaternion quat = ic->GetOrientation();
			mexPrintf("\tEasy-set: theta (deg) -> quaternion \n\t\t%f -> (%f,%f,%f,%f)\n",
				ic->GetThetaDegIC(),
				quat(1),quat(2),quat(3),quat(4));
		}
		return 1;
	}
	else if (prop == "psi-rad")
	{
		/*
		FGQuaternion Quat( propagate->GetEuler(1), propagate->GetEuler(2), value );
		Quat.Normalize();
		FGPropagate::VehicleState vstate = propagate->GetVState();
		vstate.qAttitudeLocal = Quat;
		propagate->SetVState(vstate);
		propagate->Run(false); // vVel => gamma
		auxiliary->Run(false); // alpha, beta, gamma
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: psi -> quaternion = (%f,%f,%f,%f)\n",
				propagate->GetVState().qAttitudeLocal(1),propagate->GetVState().qAttitudeLocal(2),propagate->GetVState().qAttitudeLocal(3),propagate->GetVState().qAttitudeLocal(4));
		*/
		/*
		mexPrintf("\tEasy-set: alpha (deg) = %f,\n\tbeta (deg) = %f,\n\tgamma (deg) = %f\n",
			auxiliary->Getalpha()*180./M_PI,auxiliary->Getbeta()*180./M_PI,auxiliary->GetGamma()*180./M_PI);
		*/
		ic->SetPsiRadIC(value);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
		{
			FGQuaternion quat = ic->GetOrientation();
			mexPrintf("\tEasy-set: psi (deg) -> quaternion \n\t\t%f -> (%f,%f,%f,%f)\n",
				ic->GetPsiDegIC(),
				quat(1),quat(2),quat(3),quat(4));
		}
		return 1;
	}
	else if (prop == "elevator-cmd-norm")
	{
		fdmExec->GetFCS()->SetDeCmd(value);
		fdmExec->GetFCS()->Run(false);
		//propagate->Run(false);
		//auxiliary->Run(false);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: elevator pos (deg) = %f\n",fdmExec->GetFCS()->GetDePos()*180./M_PI);
		return 1;
	}
	else if (prop == "aileron-cmd-norm")
	{
		fdmExec->GetFCS()->SetDaCmd(value);
		fdmExec->GetFCS()->Run(false);
		//propagate->Run(false);
		//auxiliary->Run(false);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: right aileron pos (deg) = %f\n",fdmExec->GetFCS()->GetDaRPos()*180./M_PI);
		return 1;
	}
	else if (prop == "rudder-cmd-norm")
	{
		fdmExec->GetFCS()->SetDrCmd(value);
		fdmExec->GetFCS()->Run(false);
		//propagate->Run(false);
		//auxiliary->Run(false);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: rudder pos (deg) = %f\n",fdmExec->GetFCS()->GetDrPos()*180./M_PI);
		return 1;
	}
	else if (prop == "set-running")
	{
		bool isrunning = false;
		if (value > 0) isrunning = true;
		for(unsigned i=0;i<fdmExec->GetPropulsion()->GetNumEngines();i++)
			fdmExec->GetPropulsion()->GetEngine(i)->SetRunning(isrunning);
		fdmExec->GetPropulsion()->GetSteadyState();
		//propagate->Run(false);
		//auxiliary->Run(false);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("\tEasy-set: engine(s) running = %d\n",(int)isrunning);
		return 1;
	}

	else if (prop == "throttle-cmd-norm")
	{
		for (int en=0; en < fdmExec->GetPropulsion()->GetNumEngines(); en++)
		{
			fdmExec->GetFCS()->SetThrottleCmd(en,value);
		}
		fdmExec->GetFCS()->Run(false);
		//propagate->Run(false);
		//auxiliary->Run(false);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
		{
			mexPrintf("\tEasy-set: all throttle cmd \n");
			std::vector<double> vs = fdmExec->GetFCS()->GetThrottleCmd();
			for(int k = 0; k < vs.size(); k++) 
			{
				mexPrintf("\t\t--> engine[%d], throttle cmd = %f\n", k, vs[k]);
			}
		}	
		return 1;
	}
	else if (prop == "mixture-cmd-norm")
	{
		for (int en=0; en < fdmExec->GetPropulsion()->GetNumEngines(); en++)
		{
			fdmExec->GetFCS()->SetMixtureCmd(en,value);
		}
		fdmExec->GetFCS()->Run(false);
		//propagate->Run(false);
		//auxiliary->Run(false);
		fdmExec->RunIC();
		if ( verbosityLevel == eVeryVerbose )
		{
			mexPrintf("\tEasy-set: all mixture cmd \n");
			std::vector<double> vs = fdmExec->GetFCS()->GetMixtureCmd();
			for(int k = 0; k < vs.size(); k++) 
			{
				mexPrintf("\t\t--> engine[%d], mixture cmd = %f\n", k, vs[k]);
			}
		}	
		return 1;
	}
	return 0;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::QueryJSBSimProperty(string prop)
{
	// mexPrintf("catalog size: %d\n",catalog.size());
	for (unsigned i=0; i<catalog.size(); i++)
	{
		// mexPrintf("__%s__\n",catalog[i].c_str());
		if (catalog[i]==prop) return 1;
	}
	return 0;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::Init(const mxArray *prhs1)
{
	// Inspired by "refbook.c"
	// The argument prhs1 is a pointer to a Matlab structure with two fields: name, value.
	// The Matlab user is forced to build such a structure first, then to pass it to the
	// mex-file. Example:
	//    >> ic(1).name = 'u'; ic(1).value = 80; (ft/s)
	//    >> ic(2).name = 'v'; ic(1).value =  0; (ft/s)
	//    >> ic(3).name = 'w'; ic(1).value =  0; (ft/s)
	//    >> ic(4).name = 'p'; ic(1).value =  0; (rad/s) % etc ...
	//    >> MexJSBSim('init', ic)

	bool success = 1;

	//*************************************************
	// Set dt=0 first
	// fdmExec->SuspendIntegration();
	//*************************************************

	const char **fnames;       /* pointers to field names */
	const mwSize *dims;
	mxArray    *tmp;
	char       *pdata=NULL;
	int        ifield, nfields;
	mxClassID  *classIDflags;
	mwIndex    jstruct;
	mwSize     NStructElems;
	mwSize     ndim;

    // get input arguments
    nfields = mxGetNumberOfFields(prhs1);
    NStructElems = mxGetNumberOfElements(prhs1);
    // allocate memory  for storing classIDflags
    classIDflags = (mxClassID*)mxCalloc(nfields, sizeof(mxClassID));

    // check empty field, proper data type, and data type consistency;
	// and get classID for each field (see "refbook.c")
    for(ifield=0; ifield<nfields; ifield++) 
	{
		for(jstruct = 0; jstruct < NStructElems; jstruct++) 
		{
			tmp = mxGetFieldByNumber(prhs1, jstruct, ifield);
			if(tmp == NULL) 
			{
				if ( verbosityLevel == eVeryVerbose )
				{
					mexPrintf("%s%d\t%s%d\n", "FIELD: ", ifield+1, "STRUCT INDEX :", jstruct+1);
					mexErrMsgTxt("Above field is empty!");
				}
				return 0;
			} 
			if(jstruct==0) 
			{
				if( (!mxIsChar(tmp) && !mxIsNumeric(tmp)) || mxIsSparse(tmp)) 
				{
					if ( verbosityLevel == eVeryVerbose )
					{
						mexPrintf("%s%d\t%s%d\n", "FIELD: ", ifield+1, "STRUCT INDEX :", jstruct+1);
						mexErrMsgTxt("Above field must have either string or numeric non-sparse data.");
					}
					return 0;
				}
				classIDflags[ifield]=mxGetClassID(tmp); 
			} 
			else 
			{
				if (mxGetClassID(tmp) != classIDflags[ifield]) 
				{
					if ( verbosityLevel == eVeryVerbose )
					{
						mexPrintf("%s%d\t%s%d\n", "FIELD: ", ifield+1, "STRUCT INDEX :", jstruct+1);
						mexErrMsgTxt("Inconsistent data type in above field!"); 
					}
					return 0;
				} 
				else if(!mxIsChar(tmp) && 
					  ((mxIsComplex(tmp) || mxGetNumberOfElements(tmp)!=1)))
				{
					if ( verbosityLevel == eVeryVerbose )
					{
						mexPrintf("%s%d\t%s%d\n", "FIELD: ", ifield+1, "STRUCT INDEX :", jstruct+1);
						mexErrMsgTxt("Numeric data in above field must be scalar and noncomplex!"); 
					}
					return 0;
				}
			}
		}
    }
    /* allocate memory for storing pointers */
    fnames = (const char **)mxCalloc(nfields, sizeof(*fnames));
    /* get field name pointers */
    for (ifield=0; ifield< nfields; ifield++)
	{
		fnames[ifield] = mxGetFieldNameByNumber(prhs1,ifield);
    }
	// At this point we have extracted from prhs1 the vector of 
	// field names fnames of nfields elements.
	// nfields is the number of fields in the passed Matlab struct (ic).
	// It may have more fields, but the first two must be "name" and "value"
	// The structure possesses generally a number of NStructElems elements.

    ndim = mxGetNumberOfDimensions(prhs1);
    dims = mxGetDimensions(prhs1);

	// loop on the element of the structure
	for (jstruct=0; jstruct<NStructElems; jstruct++) 
	{
		string prop = "";
		double value = -99.;

		// scan the fields
		// the first two must be "name" and "value"
		for(ifield=0; ifield<2; ifield++) // for(ifield=0; ifield<nfields; ifield++) // nfields=>2
		{
			tmp = mxGetFieldByNumber(prhs1,jstruct,ifield);
			if( mxIsChar(tmp) ) //  && (fnames[ifield]=="name") the "name" field
			{
				// mxSetCell(fout, jstruct, mxDuplicateArray(tmp));
				char buf[128];
				mwSize buflen;
				buflen = mxGetNumberOfElements(tmp) + 1;
				mxGetString(tmp, buf, buflen);
				prop = string(buf);
				//mexPrintf("field name: %s\n",prop.c_str());
			}
			else  // the "value" field
			{
				value = *mxGetPr(tmp);
				//mexPrintf("field value %f\n",value);
			}
		}
		//----------------------------------------------------
		// now we have a string in prop and a double in value
		// we got to set the property value accordingly
		//----------------------------------------------------
		if ( verbosityLevel == eVeryVerbose )
			mexPrintf("Property name: '%s'; to be set to value: %f\n",prop.c_str(),value);

		//----------------------------------------------------
		// Note: the time step is set to zero at this point, so that all calls 
		//       to propagate->Run() will not advance the vehicle state in time
		//----------------------------------------------------
		// Now pass prop and value to the member function
		success = success && SetPropertyValue(prop,value); // EasySet called here

		//mexPrintf("success '%d'; \n",(int)success);
    
	}// end-of-loop on the element of ic structure

	// free memory
    mxFree(classIDflags);
	mxFree((void *)fnames);

	// perform all calculations as if an FGIC were set
	fdmExec->RunIC();

	//fdmExec->ResumeIntegration();

	_h = ic->GetAltitudeASLFtIC();

	//_Vt = auxiliary->GetVt();
	_Vt = ic->GetVtrueFpsIC();
	mexPrintf("Vt = %f\n",_Vt);

	_u     = ic->GetUBodyFpsIC();
	_v     = ic->GetVBodyFpsIC();
	_w     = ic->GetWBodyFpsIC();
	_p     = ic->GetPRadpsIC();
	_q     = ic->GetQRadpsIC();
	_r     = ic->GetRRadpsIC();
	_alpha = ic->GetAlphaRadIC();
	_beta  = ic->GetBetaRadIC();
	_quat  = ic->GetOrientation();
	_phi   = ic->GetPhiRadIC();
	_theta = ic->GetThetaRadIC();
	_psi   = ic->GetPsiRadIC();
	_gamma = ic->GetFlightPathAngleRadIC();


	mexPrintf("u = %f\n",_u);
	mexPrintf("v = %f\n",_v);
	mexPrintf("w = %f\n",_w);
	mexPrintf("alpha = %f\n",_alpha);
	mexPrintf("beta  = %f\n",_beta);
	mexPrintf("p = %f\n",_p);
	mexPrintf("q = %f\n",_q);
	mexPrintf("r = %f\n",_r);
	mexPrintf("phi   = %f\n",_phi);
	mexPrintf("theta = %f\n",_theta);
	mexPrintf("psi   = %f\n",_psi);

	// see FGPropagate::Run
	FGColumnVector3 vUVWdot     = fdmExec->GetAccelerations()->GetUVWdot();
	FGColumnVector3 vPQRdot     = fdmExec->GetAccelerations()->GetPQRdot();
	FGQuaternion qQuaternionDot = fdmExec->GetAccelerations()->GetQuaterniondot();

	_udot = vUVWdot(1); // transform in body- axis components
	_vdot = vUVWdot(2);
	_wdot = vUVWdot(3);
	_pdot = vPQRdot(1);
	_qdot = vPQRdot(2);
	_rdot = vPQRdot(3);
	_q1dot = qQuaternionDot(1);
	_q2dot = qQuaternionDot(2);
	_q3dot = qQuaternionDot(3);
	_q4dot = qQuaternionDot(4);

	FGColumnVector3 euler_rates = auxiliary->GetEulerRates();
	FGColumnVector3 position_rates = propagate->GetInertialVelocity();

	_phidot   = euler_rates(1);
	_thetadot = euler_rates(2);
	_psidot   = euler_rates(3);
	_xdot     = position_rates(1);
	_ydot     = position_rates(2);
	_zdot     = position_rates(3);
	_hdot     = propagate->Gethdot();
	_alphadot = auxiliary->Getadot();
	_betadot  = auxiliary->Getbdot();

	mexPrintf("udot = %f\n",_udot);
	mexPrintf("vdot = %f\n",_vdot);
	mexPrintf("wdot = %f\n",_wdot);
	mexPrintf("pdot = %f\n",_pdot);
	mexPrintf("qdot = %f\n",_qdot);
	mexPrintf("rdot = %f\n",_rdot);

	if ( verbosityLevel == eVerbose )
	{
		mexPrintf("\tState derivatives calculated.\n");

		mexPrintf("\tV true %f (ft/s)\n",_Vt);
		mexPrintf("\t[u_dot, v_dot, w_dot] = [%f, %f, %f] (ft/s/s)\n",
			_udot,_vdot,_wdot);
		mexPrintf("\t[p_dot, q_dot, r_dot] = [%f, %f, %f] (rad/s/s)\n",
			_pdot,_qdot,_rdot);
		mexPrintf("\t[x_dot,y_dot,z_dot] = [%f, %f, %f] (ft/s)\n",
			_xdot,_ydot,_zdot);
		mexPrintf("\t[phi_dot,theta_dot,psi_dot] = [%f, %f, %f] (rad/s)\n",
			_phidot,_thetadot,_psidot);
		mexPrintf("\t[h_dot, alpha_dot, beta_dot] = [%f (ft/s/s), %f (deg/sec), %f (deg/sec)]\n",
			_hdot,_alphadot*180/M_PI,_betadot*180/M_PI);
	}

	if (!success)
	{
		if ( verbosityLevel == eVerbose )
			mexPrintf("\tERROR: One or more or all the required properties could not be initialized.\n");
		return 0;
	}
	else
		return 1;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::Init(const mxArray *prhs1, vector<double>& statedot)
{
	if (statedot.size() != 19) return 0;
	if (!Init(prhs1))
	{
		if ( verbosityLevel == eVerbose )
			mexPrintf("\tERROR: could not calculate dotted states correctly.\n");
		return 0;
	}
	statedot[ 0] = _udot;
	statedot[ 1] = _vdot;
	statedot[ 2] = _wdot;
	statedot[ 3] = _pdot;
	statedot[ 4] = _qdot;
	statedot[ 5] = _rdot;
	statedot[ 6] = _q1dot;
	statedot[ 7] = _q2dot;
	statedot[ 8] = _q3dot;
	statedot[ 9] = _q4dot;
	statedot[10] = _xdot;
	statedot[11] = _ydot;
	statedot[12] = _zdot;
	statedot[13] = _phidot;
	statedot[14] = _thetadot;
	statedot[15] = _psidot;
	statedot[16] = _hdot;
	statedot[17] = _alphadot;
	statedot[18] = _betadot;

	return 1;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bool JSBSimInterface::SetVerbosity(const mxArray *prhs1)
{
	if (!fdmExec) return 0;
	// if (!IsAircraftLoaded()) return 0;
	// even when aircraft is null we must be able to set verbosity level

	if ( mxIsChar(prhs1) )
	{
		char buf[128];
		mwSize buflen;
		buflen = mxGetNumberOfElements(prhs1) + 1;
		mxGetString(prhs1, buf, buflen);
		string prop = "";
		prop = string(buf);

		if ( (prop == "silent") ||
			 (prop == "Silent") ||
			 (prop == "SILENT")
			)
		{
			SetVerbosity( (JIVerbosityLevel)0 );
		}
		else if ( (prop == "verbose") ||
			      (prop == "Verbose") ||
			      (prop == "VERBOSE")
			)
		{
			SetVerbosity( (JIVerbosityLevel)1 );
		}
		else if ( (prop == "very verbose") ||
			      (prop == "Very Verbose") ||
			      (prop == "VERY VERBOSE")
			)
		{
			SetVerbosity( (JIVerbosityLevel)2 );
		}
		else
			return 0;
	}
	else if ( mxIsNumeric(prhs1) )
	{
		double value = 0;
		value = *mxGetPr(prhs1);
		int ival = (int) value;
		if (ival <= eVeryVerbose)
		{
			SetVerbosity( (JIVerbosityLevel)ival );
		}
		else
			return 0;
	}

	return 1;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void JSBSimInterface::PrintCatalog()
{
	if ( verbosityLevel == eVerbose )
	{
		mexPrintf("-- Property catalog for current aircraft ('%s'):\n",fdmExec->GetAircraft()->GetAircraftName());
		for (unsigned i=0; i<catalog.size(); i++)
			mexPrintf("%s\n",catalog[i].c_str());
		mexPrintf("-- end of catalog\n");
	}
	return;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
std::vector <std::string> JSBSimInterface::SPrintPropertyCatalog()
{
	return this->fdmExec->GetPropertyCatalog();
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void JSBSimInterface::PrintPropertyCatalog()
{
	// print property catalog
	std::vector <std::string> propCatalog = this->fdmExec->GetPropertyCatalog();
	mexPrintf("");
	mexPrintf("\tMexJSBSim | Property catalog for %s\n", this->fdmExec->GetModelName().c_str());
	for (unsigned i=0; i<propCatalog.size(); i++) {
		mexPrintf("\t\t%s\n", propCatalog[i].c_str());
	}

}