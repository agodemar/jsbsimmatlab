# README #

This README documents steps that are necessary to get MeXJSBSim up and running.

### What is this repository for? ###

* Quick summary:
MexJSBSim is a Mex-function that exposes JSBSim internals
to the Matlab environment.
[add more stuff here]

* Version:
0.1

### How do I get set up? ###

* Open the solution JSBSimMatlab.sln in Visual C++ 2012
* In Visual C++ IDE configure the $(MatlabRootDir) macro
via the Property Manager (Menu: View->Other Windows->Property Manager)
* Compile to obtain MexJSBSim.mexw64 in JSBSimMatlab/test

* MexJSBSim project depends on JSBSimLib projects.
JSBSimLib (Config. "JSBSim64") produces a static library JSBSimLib.lib in the directory JSBSimMatlab/x64/JSBSim64.

* How to run tests
In Matlab set the working directory to JSBSimMatlab/test and use MexJSBSim as a normal Matlab function.

* Deployment instructions (to be done)

### Contribution guidelines ###

* Writing tests (to be done)

* Code review (to be done)

* Other guidelines (to be done)

### Who do I talk to? ###

* Repo owner or admin:
agodemar

* Other community or team contact:
jary10